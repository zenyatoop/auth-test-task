# Auth test task

## Installation

+Before installing, ensure you have [Node.js](https://nodejs.org/) and [Docker](https://www.docker.com/) installed on your system.

1. Clone the repo
   ```sh
   git clone https://gitlab.com/zenyatoop/auth-test-task.git
   ```

2. cd into dir
   ```sh
   cd auth-test-task
   ```

3. Install NPM packages
   ```sh
   npm install
   ```
4. Copy .env.example into .env file

5. Run docker compose up
    ```sh
   docker compose -f docker-compose-dev.yml up -d
   ```
6. Wait till compose is running, and you can access api on port 3000 by default;
    ```
    curl localhost:<port>
    ```

## Additional info

### 1. Swagger doc is located in docs


