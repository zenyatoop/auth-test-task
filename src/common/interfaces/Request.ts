export interface RequestWithContext {
    context: Context;
}

type Context = {
    userId: number;
};
