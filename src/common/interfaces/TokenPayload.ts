export interface TokenPayload {
    userId: number;
    lastSignOut: string;
    iat: number;
    exp: number;
}
