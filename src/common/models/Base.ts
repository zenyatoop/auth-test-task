import { BeforeInsert } from 'typeorm';

export class Base {
    id: number;

    @BeforeInsert()
    private beforeInsert() {
        // generate not predictable id for db records
        this.id = this.#generateId();
    }

    #generateId() {
        const timestamp = `${Date.now()}`.slice(0, -3);

        return +`${timestamp}${this.#getRandomInt(100000, 999999)}`;
    }

    #getRandomInt(min, max) {
        const minBorder = Math.ceil(min);
        const maxBorder = Math.floor(max);

        return Math.floor(Math.random() * (maxBorder - minBorder)) + minBorder;
    }
}
