import { CanActivate, ExecutionContext, ForbiddenException, Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { User } from 'src/users/users.entity';
import { verifyToken } from 'src/auth/utils';
import { config } from 'configs/config';
import { TokenPayload } from '../interfaces/TokenPayload';

@Injectable()
export class LocalAuthGuard implements CanActivate {
    constructor(@InjectRepository(User) private userRepository: Repository<User>) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        try {
            const request = context.switchToHttp().getRequest();
            const { authorization } = request.headers;
            if (!authorization || authorization.trim() === '') {
                throw new UnauthorizedException('Please provide token');
            }
            const authToken = authorization.replace(/bearer/gim, '').trim();
            const data = verifyToken(authToken, config.secret) as TokenPayload;
            const user = await this.userRepository.findOne({ where: { id: data.userId } });

            if (!user) throw new UnauthorizedException('Wrong token');

            // handle sign out
            if (user.lastSignOutAt && new Date(data?.lastSignOut)?.getTime() !== user.lastSignOutAt?.getTime()) {
                throw new UnauthorizedException('Wrong token');
            }

            request.context = data;
            return true;
        } catch (error) {
            console.log('auth error - ', error.message);
            throw new ForbiddenException(error.message || 'session expired! Please sign In');
        }
    }
}
