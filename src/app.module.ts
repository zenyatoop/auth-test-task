import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { AuthController } from './auth/auth.controller';
import { AuthService } from './auth/auth.service';
import { User } from './users/users.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import config from 'configs/database';

// TODO: rm any--, try rx--, docker compose--, readme, push
@Module({
    imports: [AuthModule, AuthModule, TypeOrmModule.forFeature([User]), TypeOrmModule.forRoot(config), ConfigModule.forRoot()],
    controllers: [AppController, AuthController],
    providers: [AppService, AuthService]
})
export class AppModule {}
