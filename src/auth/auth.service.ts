import { HttpException, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from 'src/users/users.entity';
import { generateToken, hashPassword } from './utils';
import { config } from 'configs/config';
import * as crypto from 'crypto';
import { dumpUser } from 'src/users/users.dump';
import { Observable, catchError, from, switchMap } from 'rxjs';
import { createUserBody } from 'src/users/users.dto';

@Injectable()
export class AuthService {
    constructor(@InjectRepository(User) private userRepository: Repository<User>) {}

    async registration(data: createUserBody): Promise<object> {
        return from(this.userRepository.findOne({ where: { email: data.email } })).pipe(
            switchMap(async isUserExists => {
                if (isUserExists) throw new HttpException('Email is used', 422);

                const { salt, hashedPassword } = hashPassword(data.password, crypto.randomBytes(16).toString('hex'));

                const user: User = this.userRepository.create({ ...data, passwordHash: hashedPassword, salt });

                await this.userRepository.save(user);

                return { jwt: generateToken({ userId: user.id, lastSignOut: user.lastSignOutAt }, config.secret) };
            }),
            catchError(e => {
                if (e instanceof HttpException) throw e;
                throw new HttpException('Registration failed', 500);
            })
        );
    }

    async login(email: string, password: string): Promise<Observable<object>> {
        return from(this.userRepository.findOne({ where: { email } })).pipe(
            switchMap(async (user: User) => {
                if (!user || !(await user.checkPassword(password))) throw new UnauthorizedException();

                const token = generateToken({ userId: user.id, lastSignOut: user.lastSignOutAt }, config.secret);
                return { jwt: token };
            }),
            catchError(() => {
                throw new UnauthorizedException({
                    code: 'FORMAT_ERROR',
                    fields: { email: 'INVALID', password: 'INVALID' }
                });
            })
        );
    }

    async signOut(userId: number): Promise<object> {
        const user: User = await this.userRepository.findOne({ where: { id: userId } });

        user.lastSignOutAt = new Date();
        await this.userRepository.save(user);

        if (!user) throw new NotFoundException('User not found');

        return dumpUser(user);
    }

    async profile(userId: number): Promise<object> {
        const user: User = await this.userRepository.findOne({ where: { id: userId } });

        if (!user) throw new NotFoundException('User not found');

        return dumpUser(user);
    }
}
