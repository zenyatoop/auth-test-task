import * as crypto from 'crypto';
import * as jwt from 'jsonwebtoken';

export function hashPassword(password: string, salt: string) {
    const hashedPassword = crypto.scryptSync(password, salt, 64).toString('hex');

    return { salt, hashedPassword };
}

export function generateToken(data: object, secret: string): string {
    return jwt.sign({ ...data }, secret, { expiresIn: '24h' });
}

export function verifyToken(token: string, secret: string): string | jwt.JwtPayload {
    return jwt.verify(token, secret);
}
