import { Body, Controller, Get, Post, Req, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { createUserBody, loginUser } from 'src/users/users.dto';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from 'src/common/guards/local.auth.guard';
import { RequestWithContext } from 'src/common/interfaces/Request';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    @Post('/registration')
    @UsePipes(new ValidationPipe({ validateCustomDecorators: true, transform: true }))
    async registerUser(@Body() dataBody: createUserBody) {
        const user = await this.authService.registration(dataBody);
        return user;
    }

    @Post('/login')
    @UsePipes(new ValidationPipe({ validateCustomDecorators: true, transform: true }))
    async login(@Body() dataBody: loginUser) {
        const user = await this.authService.login(dataBody.email, dataBody.password);
        return user;
    }

    @Post('/sign-out')
    @UseGuards(LocalAuthGuard)
    @UsePipes(new ValidationPipe({ validateCustomDecorators: true, transform: true }))
    async signOut(@Req() request: RequestWithContext) {
        await this.authService.signOut(request.context.userId);

        return {};
    }

    @Get('/profile')
    @UseGuards(LocalAuthGuard)
    async profile(@Req() request: RequestWithContext) {
        return await this.authService.profile(request.context.userId);
    }
}
