export function dumpUser(user) {
    return {
        id: user.id,
        name: user.name,
        email: user.email
    };
}
