import { IsEmail, IsNotEmpty, IsStrongPassword } from 'class-validator';

export class createUserBody {
    @IsNotEmpty()
    readonly name: string;

    @IsNotEmpty()
    @IsEmail()
    readonly email: string;

    @IsNotEmpty()
    @IsStrongPassword()
    readonly password: string;
}

export class loginUser {
    @IsNotEmpty()
    @IsEmail()
    readonly email: string;

    @IsNotEmpty()
    @IsStrongPassword()
    readonly password: string;
}
