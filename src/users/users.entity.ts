// import { hashPassword } from 'src/auth/utils';
import { Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryColumn } from 'typeorm';
import * as crypto from 'crypto';
import { Base } from 'src/common/models/Base';

@Entity('Users')
export class User extends Base {
    @PrimaryColumn()
    public id: number;

    @Column({ name: 'name', type: 'varchar' })
    name: string;

    @Column({ name: 'email', type: 'varchar', unique: true })
    email: string;

    @Column({ name: 'passwordHash', type: 'varchar' })
    passwordHash: string;

    @Column({ name: 'salt', type: 'varchar' })
    salt: string;

    @Column({ name: 'lastSignOutAt', type: 'timestamp' })
    lastSignOutAt: Date;

    @CreateDateColumn({ type: 'timestamp without time zone', default: 'NOW()' })
    createdAt: Date;

    @UpdateDateColumn({ type: 'timestamp without time zone', onUpdate: 'NOW()', nullable: true })
    updatedAt: Date;

    async checkPassword(plain: string) {
        const hashedPassword = crypto.scryptSync(plain, this.salt, 64).toString('hex');

        return hashedPassword === this.passwordHash;
    }
}
