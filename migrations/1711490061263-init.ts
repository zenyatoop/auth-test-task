import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class Init1711490061263 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        return queryRunner.createTable(
            new Table({
                name: 'Users',
                columns: [
                    {
                        name: 'id',
                        type: 'bigint',
                        isPrimary: true,
                        generationStrategy: 'increment',
                        isGenerated: true
                    },
                    { name: 'name', type: 'varchar' },
                    { name: 'email', type: 'varchar', isUnique: true },
                    { name: 'passwordHash', type: 'varchar' },
                    { name: 'salt', type: 'varchar' },
                    { name: 'lastSignOutAt', type: 'timestamp', isNullable: true },
                    { name: 'createdAt', type: 'timestamp', default: 'CURRENT_TIMESTAMP(0)' },
                    { name: 'updatedAt', type: 'timestamp', default: 'CURRENT_TIMESTAMP(0)' }
                ]
            })
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        return queryRunner.dropTable('Users');
    }
}
