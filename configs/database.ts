import { ConfigModule } from '@nestjs/config';

import migrations from '../migrations';
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';
// import { User } from 'src/users/users.entity';

ConfigModule.forRoot({
    isGlobal: true
});

const config: PostgresConnectionOptions = {
    type: 'postgres',

    host: process.env.DATABASE_HOST ? process.env.DATABASE_HOST : 'localhost',
    port: process.env.DATABASE_PORT ? +process.env.DATABASE_PORT : 5432,
    username: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,

    database: process.env.DATABASE_NAME,
    // entities: [User],
    entities: [__dirname + '/../**/*.entity{.ts,.js}'],
    synchronize: process.env.DATABASE_SYNC === '1',
    extra: {
        connectionLimit: 3
    },
    migrationsTableName: 'typeorm_migrations',
    migrations: [...migrations],
    migrationsRun: true,
    logging: true
};

export default config;
