import { ConfigModule } from '@nestjs/config';

ConfigModule.forRoot();

export const config = {
    secret: process.env.SECRET
};
