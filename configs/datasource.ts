import { DataSource } from 'typeorm';
import config from './database';

const datasource = new DataSource(config);
datasource.initialize();

export default datasource;
